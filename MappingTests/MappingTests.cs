﻿using NHibernate;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Mappings;
using FluentNHibernate.Testing;
using VideoStore.Utilities;
using System.Collections;

namespace MappingTests
{
    public class MappingTests
    {
        private ISession _session;
        private Movie starWars;
        private Movie hoosiers;
                
        [SetUp]
        public void CreateSession ()
        {
            _session = SessionFactory.CreateSessionFactory().GetCurrentSession();
            _session.CreateSQLQuery("delete from videostore.Area").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Rental").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Reservation").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Customer").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Video").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Employee").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Store").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.ZipCode").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.CommunicationMethod").ExecuteUpdate();
            
            _session.CreateSQLQuery("delete from videostore.Rating").ExecuteUpdate();
            _session.CreateSQLQuery("delete from imdb.Genres where Genre = 'Tsega'").ExecuteUpdate();




            starWars = _session.Load<Movie>("tt0076759 ");
            var title = starWars.Title;

            hoosiers = _session.Load<Movie>("tt0091217 ");
            title = hoosiers.Title;
        }

        [Test]
        public void ZipCodeMappingIsCorrect()
        {
            new PersistenceSpecification<ZipCode>(_session)
                .CheckProperty(z => z.Code, "49464")
                .CheckProperty(z => z.City, "Zeeland")
                .CheckProperty(z => z.State, "Michigan");
        }

        [Test]
        public void AreaMappingIsCorrect()
        {
            var zipCodes = new List<ZipCode>
            {
                new ZipCode {
                    Code = "49423",
                    City = "Holland",
                    State = "Michigan"
                },
                new ZipCode
                {
                    Code = "48444",
                    City = "Imlay City",
                    State = "Michigan"
                }
            };

            new PersistenceSpecification<Area>(_session, new DateEqualityComparer())
                .CheckProperty(a => a.Name, "Zeeland")                
                .CheckBag(a => a.ZipCodes, zipCodes)
                .VerifyTheMappings();
        }
                       
        [Test]
        public void CustomerMappingIsCorrect()
        {
            var zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };

            var stores = new List<Store>
            {
                new Store
                {
                    StoreName = "Store 2",
                    StreetAddress = "1234 Winterwood Lane",
                    PhoneNumber = "616-748-9715",
                    ZipCode = zeeland
                },
                new Store
                {
                    StoreName = "Store 1",
                    StreetAddress = "1260 Winterwood Lane",
                    PhoneNumber = "616-123-4567",
                    ZipCode = zeeland
                },
            };
            /* Reservation res = new Reservation();
               res.Customer = new Customer();*/
            new PersistenceSpecification<Customer>(_session)
                .CheckProperty(c => c.Name, new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                })
                .CheckProperty(c => c.EmailAddress, "mcfall@hope.edu")
                .CheckProperty(c => c.StreetAddress, "27 Graves Place VWF 220")
                .CheckProperty(c => c.Password, "Abc123$!")
                .CheckProperty(c => c.Phone, "616-395-7952")
                .CheckReference(c => c.ZipCode,
                    new ZipCode()
                    {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )

                .CheckBag(
                c => c.CommunicationTypes, new List<CommunicationMethod>()
                {
                    new CommunicationMethod()
                    {
                        Frequency = 12, Name = "email", Units = CommunicationMethod.TimeUnit.Day

                    },
                    new CommunicationMethod()
                    {
                        Frequency = 13, Name = "smail", Units = CommunicationMethod.TimeUnit.Day

                    }
                }//, (Customer, CommunicationMethod) => Customer.CommunicationTypes.Add((CommunicationMethod)CommunicationMethod))
                )
                .CheckInverseList(
                    c => c.PreferredStores, stores,
                    (cust, store) => cust.AddPreferredStore(store)
                )

                //.CheckReference <Rental>(c => c.rentals, new Rental() {  })

                /*   .CheckReference<Reservation>(c => c.Reservation, new Reservation() { 
                      ReservationDate = DateTime.Now,
                      Movie = starWars
                   }*/

               
                .VerifyTheMappings();
        }
        [Test]
        public void EmployeeMappingIsCorrect() {
            ZipCode zip = new ZipCode();
            zip.Code = "0293842";
            zip.City = "Holland";
            zip.State = "MI";
            Store s = new Store();
            s.StoreName = "Store 2";
            s.StreetAddress = "1234 Winterwood Lane";
            s.PhoneNumber = "616-748-9715";
            s.ZipCode = zip;




            _session.Save(zip);
            _session.Flush();

            _session.BeginTransaction();
            _session.CreateSQLQuery("set identity_insert videostore.Store on").ExecuteUpdate();
            _session.CreateSQLQuery("insert into videostore.Store(Id, Name, StreetAddress,PhoneNumber,ZipCode) values (0,'Store 3', '123 street st', '555-555-5555','0293842')").ExecuteUpdate();
            _session.CreateSQLQuery("set identity_insert videostore.Store off").List();

            _session.CreateSQLQuery("set identity_insert videostore.Employee on").ExecuteUpdate();
            _session.CreateSQLQuery("insert into videostore.Employee(Id, First,Last,Password,Username,isTopLevel,Store,DateHired,DateOfBirth ) values (0,'Merrik','Camp','Password2','mjc',1,0,'01/04/2021','01/03/1999')").ExecuteUpdate();
            _session.CreateSQLQuery("set identity_insert videostore.Employee off").List();

            _session.GetCurrentTransaction().Commit();

            Employee Sup = _session.Get<Employee>(0);
            new PersistenceSpecification<Employee>(_session, new DateEqualityComparer())
                   .CheckProperty(e => e.Name, new Name()
                   {
                       Title = "Dr.",
                       First = "Tsega",
                       Middle = "D",
                       Last = "Tefera",
                       Suffix = "Miss"
                   })
                  .CheckProperty(e => e.Password, "123hope")
                 .CheckProperty(e => e.Username, "tsega123")
                 .CheckProperty(e => e.DateOfBirth, DateTime.Now.AddYears(-17))
                   .CheckProperty(e => e.DateHired, DateTime.Now.AddDays(-5))
                   .CheckProperty(e=>e.Supervisor,Sup)
                   .CheckReference(e => e.Store, new Store()
                   {
                       StoreName = "11 Store",
                       StreetAddress = "12354 Broke St",
                       PhoneNumber = "616-666-0000",
                       ZipCode = new ZipCode()
                       {
                           Code = "49423",
                           City = "Holland",
                           State = "Michigan"
                       }

                   })
                   .VerifyTheMappings();
        }

        [Test]
        public void StoreMappingIsCorrect()
        {
            var videos = new List<Video>()
            {
                new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now
                },
                new Video
                {
                    Movie = hoosiers,
                    NewArrival = true, 
                    PurchaseDate = DateTime.Now.AddDays(1)
                }
            };

            new PersistenceSpecification<Store>(_session)
                .CheckProperty(s => s.StoreName, "Blockbusted")
                .CheckProperty(s => s.StreetAddress, "1234 Broke Street")
                .CheckProperty(s => s.PhoneNumber, "616-666-0000")
                .CheckReference(s => s.ZipCode, 
                    new ZipCode() {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )
                .CheckInverseList( s => s.Videos, videos, (s, v) => s.AddVideo(v))
                .VerifyTheMappings();
        }

        [Test]
        public void VideoMappingIsCorrect()
        {
            new PersistenceSpecification<Video>(_session, new DateEqualityComparer())
                .CheckProperty(v => v.NewArrival, true)
                .CheckProperty(v => v.PurchaseDate, DateTime.Now)
                .CheckReference(v => v.Store, 
                    new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49423",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                )
                .CheckReference(v => v.Movie, starWars)
                .VerifyTheMappings();
        }
        [Test]
        public void CommunicationMethodMapIsCorrect()
        {
            
            var customers = new List<Customer>
            {
                new Customer
                {
                    Password = "Mcfall12",
                     EmailAddress = "mcfall@hope.edu",
                    Name = new Name() { First = "Rayn", Last = "Mcfall" },
                     ZipCode  = new ZipCode() {Code = "0293842",
                     City = "Holland",
                    State = "MI"
                },


            }};
            new PersistenceSpecification<CommunicationMethod>(_session, new DateEqualityComparer())
                .CheckProperty(c => c.Name, "Email")
               .CheckProperty(c => c.Units, CommunicationMethod.TimeUnit.Day)
                .CheckProperty(c => c.Frequency, 2)

                .CheckInverseList(
                    c => c.Customers, customers
                    ,
                    (comm, customer) => customer.Allow(comm)
                 
                )
               
        
                .VerifyTheMappings();
        }
        [Test]
        public void RatingMapIsCorrect()
        {
            new PersistenceSpecification<Rating>(_session, new DateEqualityComparer())
                .CheckProperty(r => r.Comment, "This is awsome")
                .CheckProperty(r => r.Score, 3)
                .VerifyTheMappings();
        }
        [Test]
        public void RentalMapIsCorrect()
        {
            var zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };
            new PersistenceSpecification<Rental>(_session, new DateEqualityComparer())
                .CheckProperty(r => r.RentalDate, DateTime.Now)
                .CheckProperty(r => r.ReturnDate, DateTime.Now.AddDays(2))
                .CheckProperty(r => r.DueDate, DateTime.Now.AddDays(3))
                /*.CheckReference(r => r.Rating,
                    new Rating(3)
                    {
                        Score = 3,
                        Comment = "It was good but too long"
                    }
                )*/
                .CheckReference(r => r.Video,
                    new Video
                    {
                        Movie = starWars,
                        NewArrival = false,
                       PurchaseDate = DateTime.Now,
                        Store = new Store
                        {
                            StoreName = "Store 10",
                            StreetAddress = "1234 Winterwood Lane",
                            PhoneNumber = "616-748-9715",
                            ZipCode = zeeland

                        }

                    }
                )
                .CheckReference (r => r.Customer, new Customer() { 
                      Name = new Name() {
                          Title = "Dr.",
                          First = "Ryan",
                          Middle = "L",
                          Last = "McFall",
                          Suffix = "Sr"
                      }, EmailAddress = "mcfall@hope.edu",
                    StreetAddress = "27 Graves Place VWF 220", Password = "Abc123$!", 
                    Phone = "616-395-7952", ZipCode = zeeland
                }) 
                .VerifyTheMappings();
        }
        [Test]
        public void ReservationMapIsCorrect()
        {
            Customer cust = new Customer();
            Reservation res1 = new Reservation();
            Reservation res2 = new Reservation();
            res1.Movie = starWars;
            res2.Movie = starWars;
            cust.Password = "Mcfall12";
            cust.EmailAddress = "mcfall@hope.edu";
            cust.Name = new Name() { First = "Rayn", Last = "Mcfall" };

            ZipCode zip = new ZipCode();
            zip.Code = "0293842";
            zip.City = "Holland";
            zip.State = "MI";
            cust.ZipCode = zip;
            new PersistenceSpecification<Reservation>(_session, new DateEqualityComparer())
                .CheckProperty(r => r.ReservationDate, DateTime.Now)
                .CheckReference(r=>r.Customer,cust)
                .CheckReference(r => r.Movie, starWars)
                /* .CheckInverseList(r => r.Movies, videos, (s, v) => s.AddVideo(v))*/
                .VerifyTheMappings();
        }
        [Test]
        public void GenresMappingIsCorrect()
        {
           
            new PersistenceSpecification<Genre>(_session, new DateEqualityComparer())
                .CheckProperty(g => g.Name, "Tsega")
                .VerifyTheMappings();
         }
        [Test]
        public void MovieMappingIsCorrect()
        {            
            Assert.AreEqual("tt0076759 ", starWars.TitleId);
            Assert.AreEqual("Star Wars: Episode IV - A New Hope", starWars.Title);
            Assert.AreEqual("Star Wars", starWars.OriginalTitle);
            Assert.AreEqual(1977, starWars.Year);
            Assert.AreEqual(121, starWars.RunningTimeInMinutes);
            Assert.AreEqual("PG", starWars.Rating);
            Assert.AreEqual("Action", starWars.PrimaryGenre.Name);

            /*   List<Reservation> Genre = new List<Reservation>() {
                new Reservation ()
               };*/
            //use count in the list 

            Customer cust = new Customer();
           // cust.Id = 12345;
            Reservation res1 = new Reservation();
            Reservation res2 = new Reservation();
            res1.Movie = starWars;
            res2.Movie = starWars;
            cust.Password = "Mcfall12";
            cust.EmailAddress = "mcfall@hope.edu";
            cust.Name = new Name() { First = "Rayn", Last = "Mcfall" };

            ZipCode zip = new ZipCode();
            zip.Code = "0293842";
            zip.City = "Holland";
            zip.State = "MI";
            cust.ZipCode = zip;
            res1.Customer = cust;
            res2.Customer = cust; 
            //_session.Save(cust);
            _session.Save(res1);
            _session.Save(res2);
            _session.Evict(starWars);
            starWars = _session.Get<Movie>("tt0076759 ");
            Assert.AreEqual(2, starWars.Reservations.Count);

            Assert.AreEqual(4, starWars.Genres.Count);
            Assert.AreEqual("Action", starWars.Genres[1].Name);
            Assert.AreEqual("Adventure", starWars.Genres[2].Name);
            Assert.AreEqual("Fantasy", starWars.Genres[3].Name);

        }
        [Test]
        public void HasOneCustRes() {

            // just create and save some relevant objects, then reload an object and make some assertions about it.
            Customer cust = new Customer();
            cust.Password = "Mcfall12";
            cust.EmailAddress = "mcfall@hope.edu";
            cust.Name = new Name() { First = "Rayn", Last = "Mcfall" };
         
            ZipCode zip = new ZipCode();
            zip.Code = "0293842";
            zip.City = "Holland";
            zip.State = "MI";
            cust.ZipCode = zip;

            _session.Save(cust);
           
            int idCust = cust.Id;
            Reservation res1 = new Reservation();  //add customer and movie properties, res date 
            
            res1.Movie = starWars;
            res1.ReservationDate = DateTime.Now;
            res1.Customer = cust;

            _session.Save(res1);
           // _session.Save(res2);
            _session.Evict(cust);
            cust = _session.Get<Customer>(idCust);
         // cust.Reservation = res1;
            Assert.AreEqual(res1, cust.Reservation);
            //should we be testing the other side like the resrvation with the assert thing 
        }
        [Test]
        public void HasOneRentRating()
        {
            Rating rate = new Rating();
            rate.Comment = "love it";
            rate.Score = 3; 

            

            _session.Save(rate);

            int idRate = rate.Id;
            
            ZipCode zip = new ZipCode();
            zip.Code = "0293842";
            zip.City = "Holland";
            zip.State = "MI";
            Store s = new Store();
            s.StoreName = "Store 2";
            s.StreetAddress = "1234 Winterwood Lane";
            s.PhoneNumber = "616-748-9715";
            s.ZipCode = zip;
            Video vid = new Video();
            vid.NewArrival = false;
            vid.PurchaseDate = DateTime.Now;
            vid.Store = s;
            vid.Movie = starWars;
            Rental rent = new Rental();  //add customer and movie properties, res date 
            rent.Rating = rate;
            rent.RentalDate = DateTime.Now;
            rent.DueDate = DateTime.Now.AddDays(5);
            rent.ReturnDate = DateTime.Now.AddDays(3);
            rent.Video = vid;
            
                Customer cust = new Customer();
            cust.Password = "Mcfall12";
            cust.EmailAddress = "mcfall@hope.edu";
            cust.Name = new Name() { First = "Rayn", Last = "Mcfall" };

          
            cust.ZipCode = zip;
            rent.Customer = cust;

            _session.Save(rent);
            // _session.Save(res2);
            _session.Evict(rate);
            rate = _session.Get<Rating>(idRate);
            // cust.Reservation = res1;
            Assert.AreEqual(rent, rate.Rental);
        }
        [Test]
        public void TestSupervisor()
        {
            ZipCode zip = new ZipCode();
            zip.Code = "0293842";
            zip.City = "Holland";
            zip.State = "MI";
            Store s = new Store();
            s.StoreName = "Store 2";
            s.StreetAddress = "1234 Winterwood Lane";
            s.PhoneNumber = "616-748-9715";
            s.ZipCode = zip;

            

            
            _session.Save(zip);
            _session.Flush();

            _session.BeginTransaction();
            _session.CreateSQLQuery("set identity_insert videostore.Store on").ExecuteUpdate();
            _session.CreateSQLQuery("insert into videostore.Store(Id, Name, StreetAddress,PhoneNumber,ZipCode) values (0,'Store 3', '123 street st', '555-555-5555','0293842')").ExecuteUpdate();
            _session.CreateSQLQuery("set identity_insert videostore.Store off").List();

            _session.CreateSQLQuery("set identity_insert videostore.Employee on").ExecuteUpdate();
            _session.CreateSQLQuery("insert into videostore.Employee(Id, First,Last,Password,Username,isTopLevel,Store,DateHired,DateOfBirth ) values (0,'Merrik','Camp','Password2','mjc',1,0,'01/04/2021','01/03/1999')").ExecuteUpdate();
            _session.CreateSQLQuery("set identity_insert videostore.Employee off").List();

            _session.GetCurrentTransaction().Commit();

           Employee Sup= _session.Get<Employee>(0);
            Assert.IsNull(Sup.Supervisor);



        }
    }
}
