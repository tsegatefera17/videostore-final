﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class RentalMap : ClassMap<Rental>
    {
        public RentalMap()
        {
            Id(r => r.Id);
            Map(r => r.RentalDate);
            Map(r => r.DueDate);
            Map(r => r.ReturnDate);
         //   References<Rating>(r => r.Rating, "Rating").Cascade.All();
            References<Video>(r => r.Video, "Video").Cascade.All();
            References<Customer>(r => r.Customer, "Customer").Cascade.All();
            References<Rating>(r => r.Rating, "Rating").Unique().Cascade.All();

        }
    }
}
