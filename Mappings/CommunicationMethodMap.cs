﻿using FluentNHibernate.Mapping;
using Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class CommunicationMethodMap : ClassMap<CommunicationMethod>
    {
        public CommunicationMethodMap  () {
            Id(c => c.Id);
            Map(c => c.Name);
            Map(c => c.Units);
            Map(c => c.Frequency);

            HasManyToMany<Customer>(c => c.Customers)
                .Table("CommunicationMethodToCustomer")
               // .AsList(index => index.Column("Id"))*/
                .Inverse()
                .Cascade.All();

        }
    }
}
