﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(45)]
    public class AddingCheckConstrain : Migration
    {
        public override void Down()
        {
            Execute.Sql("ALTER TABLE videostore.Employee DROP CONSTRAINT chkTop");
        }

        public override void Up()
        {
            Execute.Sql("ALTER TABLE videostore.Employee ADD CONSTRAINT chkTop CHECK ((Employee.SupervisorId is not null and Employee.IsTopLevel = 0) or (Employee.SupervisorId is null and Employee.IsTopLevel = 1)  );  ");
        }
    }
}
