﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(38)]
    public class CreateCommunicatioMethodToCustomer : Migration
    {
        public override void Down()
        {
            Delete.Table("CommunicationMethodToCustomer")
               .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("CommunicationMethodToCustomer")
             .InSchema("videostore")
             .WithColumn("Id").AsInt32().Identity().PrimaryKey()
             .WithColumn("CommunicationMethod_Id").AsInt32().NotNullable()
             .WithColumn("Customer_Id").AsInt32().NotNullable();
        }
    }
}
