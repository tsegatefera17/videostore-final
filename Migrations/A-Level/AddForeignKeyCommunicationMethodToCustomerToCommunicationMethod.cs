﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(40)]
    public class AddForeignKeyCommunicationMethodToCustomerToCommunicationMethod : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("CommunicationMethodToCustomerToCommunicationMethod")
                  .OnTable("CommunicationMethodToCustomer")
                  .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("CommunicationMethodToCustomerToCommunicationMethod")
               .FromTable("CommunicationMethodToCustomer")
               .InSchema("videostore")
               .ForeignColumn("CommunicationMethod_Id")
               .ToTable("CommunicationMethod")
               .InSchema("videostore")
               .PrimaryColumn("Id")
               .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
