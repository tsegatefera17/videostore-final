﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(44)]
    public class AddRatingToRental : Migration
    {
        public override void Down()
        {
            Delete.Column("Rating")
               .FromTable("Rental")
               .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rental")
                 .InSchema("videostore")
                 .AddColumn("Rating").AsInt32().Nullable();
        }
    }
}
