﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(41)]
    public class AddUniqueConstraintCommunicationMethodToCustomer : Migration
    {
        public override void Down()
        {
            Delete.UniqueConstraint("CommunicationMethodCustomerUnique")
                .FromTable("CommunicationMethodToCustomer")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.UniqueConstraint("CommunicationMethodCustomerUnique")
                .OnTable("CommunicationMethodToCustomer")
                .WithSchema("videostore")
                .Columns("CommunicationMethod_Id", "Customer_Id");
        }
    }
}
