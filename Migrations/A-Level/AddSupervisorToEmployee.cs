﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations.A_Level
{
    [Migration(43)]
    public class AddSupervisorToEmployee : Migration
    {
        public override void Down()
        {
            Delete.Column("IsTopLevel")
                 .FromTable("Employee")
                 .InSchema("videostore");
            Delete.Column("SupervisorId")
                .FromTable("Employee")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Employee")
                .InSchema("videostore")
                .AddColumn("SupervisorId").AsInt32().Nullable();
            Alter.Table("Employee")
                      .InSchema("videostore")
                      .AddColumn("IsTopLevel").AsBoolean().NotNullable().WithDefaultValue(0);
           ;
            


        }
    }
}
