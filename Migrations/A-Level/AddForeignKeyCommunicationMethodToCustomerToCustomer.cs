﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(39)]
    public class AddForeignKeyCommunicationMethodToCustomerToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("CommunicationMethodToCustomerToCustomer")
                 .OnTable("CommunicationMethodToCustomer")
                 .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("CommunicationMethodToCustomerToCustomer")
               .FromTable("CommunicationMethodToCustomer")
               .InSchema("videostore")
               .ForeignColumn("Customer_Id")
               .ToTable("Customer")
               .InSchema("videostore")
               .PrimaryColumn("Id")
               .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
