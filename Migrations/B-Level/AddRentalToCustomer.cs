﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
  // [Migration(38)]   NOT NEEDED 
    public class AddRentalToCustomer : Migration
    {
        public override void Down()
        {
            Delete.Column("Rentals")
               .FromTable("Customer")
               .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Customer")
               .InSchema("videostore")
               .AddColumn("Rentals")
               .AsInt32().NotNullable();
        }
    }
}
