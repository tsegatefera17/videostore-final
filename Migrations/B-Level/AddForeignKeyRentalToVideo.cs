﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
   [Migration(34)]
    public class AddForeignKeyRentalToVideo : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("RentaToVideo")
             .OnTable("Rental")
             .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("RentaToVideo")
                .FromTable("Rental")
                .InSchema("videostore")
                .ForeignColumn("Video")
                .ToTable("Video")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnUpdate(System.Data.Rule.None)
                .OnDelete(System.Data.Rule.None);
        }
    }
}
