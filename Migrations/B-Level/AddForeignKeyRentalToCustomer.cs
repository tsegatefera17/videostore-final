﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(33)]
    public class AddForeignKeyRentalToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("RentalToCustomer")
               .OnTable("Rental")
              .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("RentalToCustomer")
                   .FromTable("Rental")
                   .InSchema("videostore")
                   .ForeignColumn("Customer")
                   .ToTable("Customer")
                   .InSchema("videostore")
                   .PrimaryColumn("Id")
                   .OnUpdate(System.Data.Rule.Cascade);
/*

            Create.ForeignKey("StoreToZipCode")
                .FromTable("Store")
                .InSchema("videostore")
                .ForeignColumn("ZipCode")
                .ToTable("ZipCode")
                .InSchema("videostore")
                .PrimaryColumn("Code")
                .OnUpdate(System.Data.Rule.Cascade);*/
        }
    }
}
