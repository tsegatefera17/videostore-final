﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
  [Migration(37)]
    public class AddForeignKeyReservationToMovie : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("ReservationToMovie")
               .OnTable("Reservation")
               .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("ReservationToMovie")
             .FromTable("Reservation")
             .InSchema("videostore")
             .ForeignColumn("Movie_Id")
             .ToTable("Movies")
             .InSchema("imdb")
             .PrimaryColumn("TitleId")
             .OnUpdate(System.Data.Rule.Cascade);

          
        }
    }
}
