﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(27)]
    public class CreateReservation : Migration
    {
        public override void Down()
        {
            Delete.Table("Reservation")
             .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Reservation")
                    .InSchema("videostore")
                    .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                    .WithColumn("ReservationDate").AsDateTime();
            //do we need the id here 
                   

        }
    }
}
